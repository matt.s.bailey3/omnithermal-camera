#include <QApplication>
#include <QVector>
#include <QTime>
#include "ThermalPublisher.h"

int main( int argc, char **argv )
{
    QApplication a(argc, argv);
    ros::init(argc, argv, "thermal");

    int serialPortBaudRate = QSerialPort::Baud115200;
    QTextStream standardOutput(stdout);
    QString serialPortName;
    QSerialPort serialPort;


    serialPortName = "/dev/ttyACM0";
    serialPort.setPortName(serialPortName);
    serialPort.setBaudRate(serialPortBaudRate);

    if (!serialPort.open(QIODevice::ReadWrite)) {
        standardOutput << QObject::tr("Failed to open port %1, error: %2").arg(serialPortName).arg(serialPort.errorString()) << endl;
    }
    ThermalPublisher thermalpublisher;
    SerialPortReader serialPortReader(&serialPort);
    QObject::connect(&serialPortReader, SIGNAL(updateImage(unsigned short *,int,int)), &thermalpublisher, SLOT(updateImage(unsigned short *, int,int)));


    QTime dieTime = QTime::currentTime().addMSecs( 1000 );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }

    serialPort.write("a");

    return a.exec();
}

